package g30125.casvean.iliesebastian.l4.e8;

public class Circle extends Shape{
	private double radius=1.0;
	public static double pi=3.14;
	private String color="red";
	Circle()
	{
	
	}
	Circle(double radius)
	{
		this.radius=radius;
	}
	Circle(double radius, String color, boolean filled)
	{
		super(color,filled);
		this.radius=radius;
		
	}
	 double getRadius()
	{
		return this.radius;
	}
	 void setRadius(double radius)
	 {
		 this.radius=radius;
	 }
	double getArea()
	{
		return Math.pow(this.radius,2)*pi;
	}
	double getPerimeter()
	{
		return 2*pi*this.radius;
	}
	public String toString()
	{
		return "a circle with radius="+getRadius()+",which is a subclass of "+super.toString();
	}
	public static void main(String[] args)
	{
		Circle newCircle=new Circle(2.0,"green",false);
		System.out.println(newCircle.toString());
	}
}
