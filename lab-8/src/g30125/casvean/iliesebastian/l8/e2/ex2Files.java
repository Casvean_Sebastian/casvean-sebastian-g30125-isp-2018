package g30125.casvean.iliesebastian.l8.e2;
import java.io.*;
import java.util.*;

public class ex2Files {
	
	public static void main(String[] args) throws IOException
	{
		int check=0;
		Scanner userInput=new Scanner(System.in);
		String data;
		char getChar;
		System.out.println("Type something:");
		data=userInput.nextLine();
		File f=new File("E:\\\\WORKSPACE\\\\Lab7Rezolvat\\\\src\\\\g30125\\\\casvean\\\\iliesebastian\\\\l8\\\\e2\\\\data.txt");
		// --- OPTIONAL ----
		try {
		PrintWriter fileW = new PrintWriter(new BufferedWriter(new FileWriter(f)));
		fileW.println(data);
		fileW.close();
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		// --- OPTIONAL ---
		System.out.println("What character are you looking for? ");
		getChar=userInput.next().charAt(0);
		char[] Converted;
		try {
			BufferedReader FileR = new BufferedReader(new FileReader(f));
			String catchLine;
			catchLine=FileR.readLine();
			while(catchLine != null)
			{
				Converted=catchLine.toCharArray();
				for(int i=0;i<Converted.length;i++)
				{
					if(Converted[i]==getChar)
					check++;
				}
				catchLine=FileR.readLine();
			}
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		System.out.println("Number of"+" "+getChar+"'s"+"="+check);
		
	}
}
