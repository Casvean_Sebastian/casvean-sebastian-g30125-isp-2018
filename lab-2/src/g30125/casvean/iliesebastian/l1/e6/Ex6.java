package g30125.casvean.iliesebastian.l1.e6;
import java.util.*;
//First Method 


/*public class Ex6 {
	static Scanner userAdd=new Scanner(System.in);
	public static void main (String[] Args)
	{
		int N,aux,result=1;
		System.out.print("Give the number: ");
		N=userAdd.nextInt();
		aux=N;
		if(N==0)
		{
			result=1;
		}
		else
		{
		while(N!=0)
		{
			result*=N;
			N--;
		}
		}
		System.out.println(aux+"!="+result);
			
	}

}
*/



//Second Method

public class Ex6{
	static Scanner userAdd=new Scanner(System.in);
	public static int recursive(int N)
	{
		if(N==0) return 1;
		return N*recursive(N-1);
	}
	public static void main(String[] args)
	{
		int N,result;
		System.out.print("N= ");
		N=userAdd.nextInt();
		result=recursive(N);
		System.out.println(N+"!="+result);
	}
}
