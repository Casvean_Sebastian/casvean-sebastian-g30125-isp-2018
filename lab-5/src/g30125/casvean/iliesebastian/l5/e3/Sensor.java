package g30125.casvean.iliesebastian.l5.e3;

public abstract class Sensor {
	private String location;
	public Sensor()
	{
		
	}
	public abstract int readValue();
	public String getLocation()
	{
		return this.location;
	}

}
