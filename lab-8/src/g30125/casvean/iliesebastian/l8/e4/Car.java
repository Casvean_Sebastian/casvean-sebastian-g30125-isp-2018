package g30125.casvean.iliesebastian.l8.e4;

import java.io.Serializable;

public class Car implements Serializable{
	private String model;
	private int price;
	public Car(String model, int price)
	{
		this.model=model;
		this.price=price;
	}
	public int getPrice()
	{
		return this.price;
		
	}
	public String getModel()
	{
		return this.model;
	}
	public boolean equals(Object o)
	{
		if(o instanceof Car)
		{
			Car c=(Car) o;
			if(model==c.model && price==c.price)
			return true;
		}
		return false;
	}

}
