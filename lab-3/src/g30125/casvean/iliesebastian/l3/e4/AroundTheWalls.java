package g30125.casvean.iliesebastian.l3.e4;

import g30125.casvean.iliesebastian.l3.e3.City;
import g30125.casvean.iliesebastian.l3.e3.Robot;
import g30125.casvean.iliesebastian.l3.e3.Wall;

public class AroundTheWalls {
	public static void main(String[] args)
	{
		City Cluj=new City();
		Wall block1=new Wall(Cluj,1,1,Direction.WEST);
		Wall block2=new Wall(Cluj,2,1,Direction.WEST);
		Wall block3=new Wall(Cluj,2,1,Direction.SOUTH);
		Wall block4=new Wall(Cluj,2,2,Direction.SOUTH);
		Wall block5=new Wall(Cluj,2,2,Direction.EAST);
		Wall block6=new Wall(Cluj,1,2,Direction.EAST);
		Wall block7=new Wall(Cluj,1,1,Direction.NORTH);
		Wall block8=new Wall(Cluj,1,2,Direction.NORTH);
	    Robot karel = new Robot(Cluj, 0, 2, Direction.WEST);
	    for(int i=0;i<2;i++)
		{
	    	karel.move();
		}
	    karel.turnLeft();
	    for(int i=0;i<3;i++)
		{
	    	karel.move();
		}
	    karel.turnLeft();
	    for(int i=0;i<3;i++)
		{
	    	karel.move();
		}
	    karel.turnLeft();
	    for(int i=0;i<3;i++)
		{
	    	karel.move();
		}
	    karel.turnLeft();
	    karel.move();
	}
}