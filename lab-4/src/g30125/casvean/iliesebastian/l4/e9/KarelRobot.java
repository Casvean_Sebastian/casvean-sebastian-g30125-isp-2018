package g30125.casvean.iliesebastian.l4.e9;

import becker.robots.*;

public class KarelRobot
{
	static void Left (Robot test)
	{
		test.turnLeft();
		test.turnLeft();
		test.turnLeft();
	}
	static void Dive(Robot test)
	{
		int i=1;
		while(i<=12)
		{
			test.turnLeft();
			i++;
		}
	}
   public static void main(String[] args)
   {  
   	  City prague = new City();
      Robot karel = new Robot(prague, 3, 3, Direction.NORTH);
      Wall blockAve1 = new Wall(prague, 4, 3, Direction.NORTH);
      Wall blockAve2 = new Wall(prague, 4, 2, Direction.EAST);
      Wall blockAve3 = new Wall(prague, 5, 2, Direction.EAST);
      Wall blockAve4 = new Wall(prague, 6, 2, Direction.EAST);
      Wall blockAve5 = new Wall(prague, 6, 3, Direction.WEST);
      Wall blockAve6 = new Wall(prague, 6, 3, Direction.SOUTH);
      Wall blockAve7 = new Wall(prague, 6, 4, Direction.SOUTH);
      Wall blockAve8 = new Wall(prague, 6, 4, Direction.EAST);
      karel.move();
      Left(karel);
      karel.move();
      Left(karel);
      karel.move();
      Dive(karel);
      karel.move();
      karel.move();
      karel.move();
      karel.turnLeft();
      karel.turnLeft();
      
      
      
   }
} 