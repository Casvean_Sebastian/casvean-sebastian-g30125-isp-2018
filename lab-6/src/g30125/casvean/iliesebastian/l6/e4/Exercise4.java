package g30125.casvean.iliesebastian.l6.e4;

import java.util.Arrays;

public class Exercise4 implements CharSequence{


    private char[] chars;

    public Exercise4(char[] chars) {
        this.chars = chars;
    }

    @Override
    public int length() {
        return chars.length;
    }

    @Override
    public char charAt(int index) {
        return chars[index];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        char[] newc = Arrays.copyOfRange(chars,start,end);
        return new Exercise4(newc);
    }

}
