package g30125.casvean.iliesebastian.l5.e2;

public class ProxyImage implements Image{
	   private RealImage realImage;
	   private RotatedImage rotatedImage;
	   private String fileName;
	   private char display;
	   
	   public ProxyImage(String fileName,char display){
	      this.fileName = fileName;
	      this.display=display;
	   }
	   
	   public void display() {
	      if(this.display=='d')
	      {
	    	  realImage=new RealImage(fileName);
	    	  realImage.display();
	      }
	      else
	      {
	    	  rotatedImage=new RotatedImage(fileName);
	    	  rotatedImage.display();
	      }
	   }
	  
	   public static void main(String[] args)
	   {
		  ProxyImage test=new ProxyImage("Hello",'d');
		  test.display();
		  
	   }
}
