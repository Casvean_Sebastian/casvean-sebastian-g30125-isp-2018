package g30125.casvean.iliesebastian.l5.e3;


public class Controller {
	private TemperatureSensor s1;
	private LightSensor s2;
	public Controller()
	{
		s1=new TemperatureSensor();
		s2=new LightSensor();
	}
	public void control() throws InterruptedException
	{
		int i=20;
		while(i>0)
		{
		System.out.println("Temperature="+s1.readValue());
		System.out.println("Luminosity="+s2.readValue());
		System.out.println();
		Thread.sleep(1000);
		i--;
		}
	}
	
}
