package g30125.casvean.iliesebastian.l1.e7;
import java.util.*;
public class Ex7 {
	static Scanner userAdd=new Scanner(System.in);
	public static void main(String[] args)
	{
		int guestNumber,k=3,randomNumber;
		randomNumber=(int)(Math.random()*11);
		System.out.println("Guess a number between 0 and 10");
		while(k!=0)
		{
			System.out.println("Type your number: ");
			guestNumber=userAdd.nextInt();
			if(guestNumber==randomNumber)
			{
				System.out.println("You've guessed the number! Good job!");
				break;
			}
			else if(guestNumber<randomNumber)
			{
			System.out.println("The random number is bigger than " + guestNumber);
			k--;
			}
			else if(guestNumber>randomNumber)
			{
			System.out.println("The random number is less than " +guestNumber);
			k--;
			}
			else 
			{
				k--;
				if(k==0) break;
				else
				{
				System.out.println("Try again!");
				}
			}
		}
		if(k==0)
		{
			System.out.println("Game over!");
			System.out.println("The number was: "+randomNumber);
		}
		
	}

}
