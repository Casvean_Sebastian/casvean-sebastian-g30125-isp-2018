package g30125.casvean.iliesebastian.l6.e1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    private boolean filled;
    private int xCoord;
    private int yCoord;
    private String ID;

    public Shape(Color color,boolean filled,int xCoord,int yCoord,String ID) {
        this.color = color;
        this.filled=filled;
        this.xCoord=xCoord;
        this.yCoord=yCoord;
        this.ID=ID;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    int getX()
    {
    	return xCoord;
    }
    int getY()
    {
    	return yCoord;
    }
    String getID()
    {
    	return ID;
    }
    boolean isFilled()
    {
    	return this.filled;
    }

    public abstract void draw(Graphics g);
}