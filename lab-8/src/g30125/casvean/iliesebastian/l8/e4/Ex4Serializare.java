package g30125.casvean.iliesebastian.l8.e4;
import java.io.*;
import java.util.*;

public class Ex4Serializare {
	static ArrayList<Car> cars=new ArrayList<>();
	public static void main(String[] args) throws IOException
	{
		Scanner userInput=new Scanner(System.in);
		int pick;
		File f = new File("cars.txt");
		while(true)
		{
			System.out.println("1.Create car");
			System.out.println("2.Add cars to file");
			System.out.println("3.List all cars from file");
			System.out.println("4.Display details");
			System.out.println("0.Exit");
			pick=userInput.nextInt();
			switch(pick)
			{
			case 0:
				System.exit(0);
			case 1:{
				String model;
				int price;
				System.out.println("Model=");
				model=userInput.next();
				System.out.println("Price=");
				price=userInput.nextInt();
				Car c = new Car(model,price);
				cars.add(c);
				break;
			}
			case 2:
			{
				try {
				ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream(f));
				for(int i=0;i<cars.size();i++)
				{
					oos.writeObject(cars.get(i));
				}
				oos.close();
				}catch(IOException e)
				{
					e.printStackTrace();
				}
				System.out.println("SUCCES! The cars have been added to the file!");
				break;
			}
			case 3:
			{
				boolean cond=true;
				try {
					ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
					for(int i=0;i<cars.size();i++)
					{
						Car c=(Car)ois.readObject();
						System.out.println(c);
					}
				
					ois.close();
					
				}catch(ClassNotFoundException e)
				{
					e.printStackTrace();
				}catch(IOException e)
				{
					e.printStackTrace();
				}
				
				break;
			}
			case 4:
			{
				try {
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
				for(int i=0;i<cars.size();i++)
				{
					Car c=(Car)ois.readObject();
					System.out.println("Model: "+c.getModel());
					System.out.println("Price: "+c.getPrice());
					System.out.println();
				}
				}catch(ClassNotFoundException e)
				{
					e.printStackTrace();
				}catch(IOException e)
				{
					e.printStackTrace();
				}
			}
			}
			
		}
		
		
		
	}

}
