package g30125.casvean.iliesebastian.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public interface Shape {
	public Color getColor();
	public boolean isFilled();
	public String getID();
 void draw(Graphics g);
	
	
}