package g30125.casvean.iliesebastian.l4.e2;

public class MyPoint {
	private int x,y;
	public MyPoint()
	{
		this.x=0;
		this.y=0;
	}
	public MyPoint(int xCoord,int yCoord)
	{
		this.x=xCoord;
		this.y=yCoord;
	}
	public int getX()
	{
		return this.x;
	}
	public int getY()
	{
		return this.y;
	}
	public void setXY(int x,int y)
	{
		this.x=x;
		this.y=y;
	}
	public String ToString()
	{
		return "("+this.x+","+this.y+")";
	}
	public double distance(int x,int y)
	{
		double sum1=0,sum2=0;
		sum2=Math.pow(x-this.x,2) + Math.pow(y-this.y, 2);
		sum1=Math.sqrt(sum2);
		return sum1;
		
	}
	public double distance(MyPoint anotherPoint)
	{
		double sum1=0,sum2=0;
		sum2=Math.pow(anotherPoint.getX()-this.x, 2)+Math.pow(anotherPoint.getY()-this.y,2);
		sum1=Math.sqrt(sum2);
		return sum1;
	}
}