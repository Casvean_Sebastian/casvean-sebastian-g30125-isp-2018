package g30125.casvean.iliesebastian.l4.e8;

public class Square extends Rectangle{
	Square()
	{
		
	}
	Square(double side)
	{
		super(side,side);	
	}
	Square(double side,String color,boolean filled)
	{
		super(side,side,color,filled);
	}
	double getSide()
	{
		return super.getWidth();
	}
	void setSide(double side)
	{
		super.setWidth(side);
		super.setLength(side);
	}
	public String toString()
	{
		return "A square with side="+getSide()+" which is a subclass of "+super.toString();
	}
	public static void main(String[] args)
	{
		Square testsquare=new Square(2);
		System.out.println(testsquare.toString());
	}

}
