package g30125.casvean.iliesebastian.l4.e7;

public class Cylinder extends Circle{
	double height = 1.0;
	Cylinder()
	{
		
	}
	Cylinder(double radius)
	{
		super(radius);
	}
	Cylinder(double radius, double height)
	{
		super(radius);
		this.height=height;
	}
	double getHeight()
	{
		return this.height;
	}
	double getVolume()
	{
		return pi*Math.pow(getRadius(), 2)*this.height;
	}
	public static void main(String[] args)
	{
		Cylinder first= new Cylinder(1,3);
		System.out.println(first.getVolume());
		
	}

}
