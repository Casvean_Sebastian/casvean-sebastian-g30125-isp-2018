package g30125.casvean.iliesebastian.l5.e1;

import static org.junit.Assert.assertEquals;

import org.junit.*;


public class TestClass {
	private Shape[] shapes=new Shape[3];
	@Before
	public void setUp()
	{
		for(int i=0;i<shapes.length;i++)
		{
			int randomNumber=(int)(Math.random()*50);
			if(randomNumber <10)
				shapes[i]=new Circle(3,"blue",false);
			else if(randomNumber >=10 && randomNumber < 20)
				shapes[i]=new Square(4,"red",false);
			else
				shapes[i]=new Rectangle(5,6,"green",true);
		}
	}
	@Test
	public void shouldReturnType()
	{
			for(int i=0;i<shapes.length;i++)
			{
				
				if(shapes[i] instanceof Circle)
				{
			  assertEquals("Circle",shapes[i].toString()); 
			  continue;
				}
				else if(shapes[i] instanceof Square)
				{
			  assertEquals("Square",shapes[i].toString());
			  continue;
				}
				else if(shapes[i] instanceof Rectangle)
				{
					assertEquals("Rectangle",shapes[i].toString());
					continue;
				}
		
			}
	}
	
	@Test
	public void ShouldReturnArea()
	{
		
		for(int i=0;i<shapes.length;i++)
		{
			if(shapes[i] instanceof Circle)
			{
				assertEquals(28.274333882308138,shapes[i].getArea(),0.01);
				continue;
			}
			else if(shapes[i] instanceof Rectangle)
			{
				assertEquals(30,shapes[i].getArea(),0);
				continue;
			}
			else if(shapes[i] instanceof Square)
			{
				assertEquals(16,shapes[i].getArea(),0);
				continue;
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}


