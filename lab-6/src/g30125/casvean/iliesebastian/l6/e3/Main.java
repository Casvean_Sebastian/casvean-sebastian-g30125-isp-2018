package g30125.casvean.iliesebastian.l6.e3;

import java.awt.Color;

public class Main {
	public static void main(String[] args)
	{
		DrawingBoard b1 = new DrawingBoard();
		Shape s1=new Circle(Color.RED,true,100,100,"Circle 1",40);
		b1.addShape(s1);
		Shape s2=new Circle(Color.BLUE,true,100,160,"Circle 2",40);
		b1.addShape(s2);
		
	}
}
