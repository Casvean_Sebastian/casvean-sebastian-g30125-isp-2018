package g30125.casvean.iliesebastian.l3.e5;

public class Newspaper {
	public static void main(String[] args)
	{
		City Cluj=new City();
		Wall block1=new Wall(Cluj,1,1,Direction.WEST);
		Wall block2=new Wall(Cluj,2,1,Direction.WEST);
		Wall block3=new Wall(Cluj,2,1,Direction.SOUTH);
		Wall block4=new Wall(Cluj,1,2,Direction.SOUTH);
		Wall block6=new Wall(Cluj,1,2,Direction.EAST);
		Wall block7=new Wall(Cluj,1,1,Direction.NORTH);
		Wall block8=new Wall(Cluj,1,2,Direction.NORTH);
		Robot karel=new Robot(Cluj,1,2,Direction.SOUTH);
		Thing newspaper=new Thing(Cluj,2,2);
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.move();
		karel.pickThing();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
	}
}