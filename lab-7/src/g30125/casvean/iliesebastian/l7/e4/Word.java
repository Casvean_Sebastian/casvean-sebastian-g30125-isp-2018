package g30125.casvean.iliesebastian.l7.e4;

import java.util.Objects;

public class Word {
	private String name;
	public Word(String name)
	{
		this.name=name;
	}
	public String toString()
	{
		return this.name;
	}
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Word)) return false;
        Word w = (Word) o;
        return Objects.equals(name,w.name);
    }
	
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
