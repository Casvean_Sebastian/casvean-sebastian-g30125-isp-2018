package g30125.casvean.iliesebastian.l1.e2;

import java.util.*;
//Method 1

/*public class Ex2 {
	static Scanner userAdd=new Scanner(System.in);
	public static void main(String[] args)
	{
		int displayNumber;
		System.out.print("What number do you want to display?: ");
		displayNumber=userAdd.nextInt();
		switch(displayNumber)
		{
		case 0: System.out.println("Zero"); break;
		case 1: System.out.println("One"); break;
		case 2: System.out.println("Two"); break;
		case 3: System.out.println("Three"); break;
		case 4: System.out.println("Four"); break;
		case 5: System.out.println("Five"); break;
		case 6: System.out.println("Six"); break;
		case 7: System.out.println("Seven"); break;
		case 8: System.out.println("Eight"); break;
		case 9: System.out.println("Nine"); break;
		default: System.out.println("Wrong number!"); break;
		
		}
		
	}

}
*/


//Method 2

public class Ex2{
	static Scanner userAdd=new Scanner(System.in);
	public static void main(String[] args)
	{
		int value;
		System.out.print("Type your number: ");
		value=userAdd.nextInt();
		if(value==0)
			System.out.println("Zero");
		else if(value ==1)
			System.out.println("One");
		else if(value==2)
			System.out.println("Two");
		else if(value==3)
			System.out.println("Three");
		else if(value==4)
			System.out.println("Four");
		else if(value==5)
			System.out.println("Five");
		else if(value==6)
			System.out.println("Six");
		else if(value==7)
			System.out.println("Seven");
		else if(value==8)
			System.out.println("Eight");
		else if(value==9)
			System.out.println("Nine");
		else System.out.println("Error!");
			
		
	}
	
}

