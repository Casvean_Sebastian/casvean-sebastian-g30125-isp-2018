package g30125.casvean.iliesebastian.l6.e1;

import java.awt.*;


public class Main {
    public static void main(String[] args) {
        DrawingBoard b1=new DrawingBoard();
        Shape s1=new Circle(Color.RED,true,10,10,"Shape 1",40);
        b1.addShape(s1);
        Shape s2=new Circle(Color.BLUE,false,20,20,"Shape 2",30);
        b1.addShape(s2);
        Shape s3=new Rectangle(Color.BLACK,true,50,50,"Shape 3",6,8);
        b1.addShape(s3);
        
        b1.deleteShape("Shape 3");
        Shape s4=new Rectangle(Color.BLACK,true,100,100,"Shape 4",10,10);
        b1.addShape(s4);
        DrawingBoard b2=new DrawingBoard();
        b2.addShape(s1);
        
        
    }
}
