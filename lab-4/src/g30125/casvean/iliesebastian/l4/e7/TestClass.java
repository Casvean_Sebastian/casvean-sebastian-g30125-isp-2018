package g30125.casvean.iliesebastian.l4.e7;

import org.junit.*;
import static org.junit.Assert.*;

public class TestClass {
	private Circle Test=new Circle(20);
	private Cylinder CylTest=new Cylinder(20,13);
	@Test
	public void shouldReturnRadius()
	{
		assertEquals(20,Test.getRadius(),0);
	}
	@Test
	public void shouldReturnHeight()
	{
		assertEquals(13,CylTest.getHeight(),0);
	}
	
}
