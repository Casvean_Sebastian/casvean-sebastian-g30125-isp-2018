package g30125.casvean.iliesebastian.l4.e6;
import g30125.casvean.iliesebastian.l4.e6.Author;

public class Book {
	private String name;
	private Author[] authors;
	private double price;
	private int qtyInStock=0;
	private int numberOfAuthors=0;
	Book(String name, Author[] authors, double price)
	{
		this.numberOfAuthors=authors.length;
		this.name=name;
		this.authors=authors;
		this.price=price;
	}
	 Book(String name, Author[] authors, double price, int qtyInStock)
	{
		this.numberOfAuthors=authors.length;
		this.name=name;
		this.authors=authors;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	int getNOA()
	{
		return this.numberOfAuthors;
	}
	String getName()
	{
		return this.name;
	}
	Author[] getAuthors()
	{
		return this.authors;
	}
	double getPrice()
	{
		return this.price;
	}
	void setPrice(double price)
	{
		this.price=price;
	}
	int getQtyInStock()
	{
		return this.qtyInStock;
	}
	void setQtyInStock(int qtyInStock)
	{
		this.qtyInStock=qtyInStock;
	}
	public String toString()
	{
		return getName()+" by "+getNOA()+" authors";
	}
	void printAuthors()
	{
		int i=0;
		for(i=0;i<getAuthors().length;i++)
		{
			System.out.println(getAuthors()[i].getName());
		}
	}
	public static void main(String[] args)
	{
		Author[] authors = new Author[2];
		authors[0]=new Author("carag","mist@yahoo",'m');
		authors[1]=new Author("caterina","cater@yahoo",'f');
		Book newBook=new Book("By the tree",authors,25.99,3);
		System.out.println(newBook.getNOA());
		System.out.println(newBook.toString());
		newBook.printAuthors();
	}
}