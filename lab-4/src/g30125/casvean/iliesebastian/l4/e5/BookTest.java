package g30125.casvean.iliesebastian.l4.e5;
import org.junit.*;

import g30125.casvean.iliesebastian.l4.e4.Author;

import static org.junit.Assert.*;

public class BookTest {
	private Book booktest;
	private Book booktest2;
	private Author author1 = new Author("Caragiale","caragiale@yahoo.com",'m');
	@Before
	public void setUp()
	{
		booktest = new Book("In ape adanci",author1,43.535);
		booktest2= new Book("Dansu ploii",author1,19.99,3);
	}
	@Test
	public void shouldGetName()
	{
		assertEquals("In ape adanci",booktest.getName());
	}
	@Test
	public void shouldGetAuthor()
	{
		assertEquals(author1,booktest.getAuthor());
	}
	@Test
	public void shouldReturnPrice()
	{
		assertEquals(43.535,booktest.getPrice(),0);
	}
	@Test
	public void shouldSetPrice()
	{
		booktest.setPrice(19);
		assertEquals(19,booktest.getPrice(),0);
	}
	@Test
	public void shouldGetQty()
	{
		assertEquals(3,booktest2.getQtyInStock());
	}
	
	@Test
	public void shouldSetQty()
	{
		booktest2.setQtyInStock(30);
		assertEquals(30,booktest2.getQtyInStock());
	}
}
