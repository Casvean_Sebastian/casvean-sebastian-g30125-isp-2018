package g30125.casvean.iliesebastian.l7.e4;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;

public class Dictionary extends JFrame{
	private HashMap<Word,Definition> Dictionar=new HashMap<>();
	private JList list;
	private JList list2;
	private JButton button;
	private JTextField textField;
	private static ArrayList<Word> wordslist =new ArrayList<>();
	private static ArrayList<Definition> definitionslist=new ArrayList<>();
	private static String[] words;
	private static String[] definitions;
	public Dictionary()
	{
		
	}
	public Dictionary (String hello)
	{
		this.setTitle("Dictionary");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600,400);
		setLayout(new FlowLayout());
		list=new JList(words);
		
		list.setVisibleRowCount(4);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		
		this.add(new JScrollPane(list));
		button=new JButton("Get definition!");
		button.addActionListener(new ActionListener() { 
			  public void actionPerformed(ActionEvent e) { 
			    if(e.getSource()==button)
			    {
			    	
			    	textField.setText(definitions[list.getSelectedIndex()]);
			    }
			  } 
			} );
		this.add(button);
		textField=new JTextField("Here you'll get the definition of the selected word:");
		textField.setPreferredSize( new Dimension( 200, 200 ) );
		this.add(new JScrollPane(textField));
		
		this.setVisible(true);
	}
	public void addWord(Word w,Definition d)
	{
		Dictionar.put(w, d);
		wordslist.add(w);
		definitionslist.add(d);
	}
	public Definition getDefinition(Word w)
	{
	      Set set = Dictionar.entrySet();
	      Iterator i = set.iterator();
	      while(i.hasNext()) {
	         Map.Entry me = (Map.Entry)i.next();
	         if(me.getKey().equals(w))
	         
	        	 return (Definition)me.getValue();
	         
	      }
	      return null;
	}
	public void getAllWords()
	{
		 Set set = Dictionar.entrySet();
	      Iterator i = set.iterator();
	      System.out.println("Available words: ");
	      while(i.hasNext()) {
	         Map.Entry me = (Map.Entry)i.next();
	         System.out.println(me.getKey().toString());
	      }
	}
	public void getAllDefinitions()
	{
		Set set = Dictionar.entrySet();
	      Iterator i = set.iterator();
	      System.out.println("Definitions: ");
	      while(i.hasNext()) {
	         Map.Entry me = (Map.Entry)i.next();
	         System.out.println(me.getValue().toString());
	      }
	}
	public static void main(String[] args)
	{
		Dictionary d=new Dictionary();
		d.addWord(new Word("Cal"),new Definition("Animal"));
		d.addWord(new Word("Pisica"), new Definition("Tot animal"));
		System.out.println(d.getDefinition(new Word("Cal")));
		words= new String[wordslist.size()];
		for(int i=0;i<wordslist.size();i++)
		{
			words[i]=wordslist.get(i).toString();
		}
		definitions=new String[wordslist.size()];
		for(int i=0;i<wordslist.size();i++)
		{
			definitions[i]=definitionslist.get(i).toString();
		}
		
		new Dictionary("1");
	}
	}
	

