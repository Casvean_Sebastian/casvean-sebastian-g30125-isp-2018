package g30125.casvean.iliesebastian.l7.e1;
import java.util.Objects;
public class BankAccount implements Comparable{
	private String owner;
	private double balance;
	public BankAccount(String owner, double balance)
	{
		this.owner=owner;
		this.balance=balance;
	}
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BankAccount)) return false;
        BankAccount account1 = (BankAccount) o;
        return Objects.equals(owner,account1.owner);
    }
	
    @Override
    public int hashCode() {
        return Objects.hash(owner);
    }
    
    public void withdraw(double ammount)
    {
    	if(this.balance-ammount>0)
    		this.balance-=ammount;
    	else
    		System.out.println("Not enough money !");
    }
    public void deposit(double ammount)
    {
    	this.balance+=ammount;
    }
    public double getBalance()
    {
    	return this.balance;
    }
    public String getOwner()
    {
    	return this.owner;
    	
    }
    public static void main(String[] args)
    {
    	BankAccount account1=new BankAccount("Owner1",125.34);
    	BankAccount account2=new BankAccount("Owner2",123.599);
    	BankAccount account3=new BankAccount("Owner1",3958.33);
    	if(account1.equals(account2))
    		System.out.println("ACELASI OWNER");
    	else System.out.println("OWNER DIFERIT");
    	
    	if(account1.equals(account3))
    		System.out.println("ACELASI OWNER");
    	else System.out.println("OWNER DIFERIT");
    		
    }
	@Override
	public int compareTo(Object o) 
	{
		BankAccount check=(BankAccount) o;
        if(this.balance>check.balance) return 1;
        if(this.balance==check.balance) return 0;
        return -1; 
			
	}
	

}
