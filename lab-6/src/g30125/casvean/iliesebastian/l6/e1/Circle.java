package g30125.casvean.iliesebastian.l6.e1;
import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color, boolean filled,int xCoord,int yCoord,String ID,int radius) {
        super(color,filled,xCoord,yCoord,ID);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(this.getX(),this.getY(),radius,radius);
        if(this.isFilled())
        {
        	g.fillOval(this.getX(),this.getY(), radius, radius);
        }
    }
}
