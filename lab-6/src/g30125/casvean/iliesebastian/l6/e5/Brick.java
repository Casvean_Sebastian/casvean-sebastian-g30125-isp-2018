package g30125.casvean.iliesebastian.l6.e5;

import java.awt.Graphics;

public class Brick implements Shape{
	private int length;
	private int xcoord,ycoord;
	private int width;
	public Brick(int xcoord,int ycoord,int length,int width)
	{
		this.xcoord=xcoord;
		this.ycoord=ycoord;
		this.length=length;
		this.width=width;
	}
	int getLength()
	{
		return this.length;
	}
	int getWidth()
	{
		return this.width;
	}
	
	public void draw(Graphics g)
	{
		g.drawRect(xcoord,ycoord, length,width);
	}

}
