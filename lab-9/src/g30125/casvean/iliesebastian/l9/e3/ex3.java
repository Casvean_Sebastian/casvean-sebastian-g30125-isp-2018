package g30125.casvean.iliesebastian.l9.e3;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.io.IOException;
import java.awt.*;

public class ex3 extends JFrame{
	private JLabel file;
	private JTextField fileName;
	private JTextArea content;
	private JButton click;
	ex3()
	{
		this.setTitle("Files");
		this.setSize(400,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		try{
		init();
		}catch(IOException e)
		{
			e.printStackTrace();
		}
		this.setVisible(true);
	}
	public void init () throws IOException
	{
		this.setLayout(null);
		int width=80;int height = 20;
		file=new JLabel("FName:");
		file.setBounds(10, 50, width, height);
		fileName=new JTextField("");
		fileName.setBounds(70,50,width, height);
		content=new JTextArea("");
		content.setBounds(20,100,150,150);
		click=new JButton("GetInfo");
		click.setBounds(170,50,width,height);
		
		click.addActionListener(new ActionListener(){

			
			public void actionPerformed(ActionEvent e) {
				if(e.getSource()==click)
				{
					String file;
					file=ex3.this.fileName.getText();
					File f=new File(file);
					if(!f.exists())
						ex3.this.content.append("Fisierul nu exista!");
					else
					{
						try{
						DataInputStream dis=new DataInputStream(new FileInputStream(f));
						String info;
						info=dis.readLine();
						while(info!=null)
						{
							ex3.this.content.append(info+"\n");
							info=dis.readLine();
						}
						}catch(IOException ev)
						{
							ev.printStackTrace();
						}
						
					}
					
				}
				
			}
			
		});
		add(file);
		add(fileName);
		add(content);
		add(click);
		
        
        
	}
	public static void main(String[] args)
	{
		new ex3();
	}
}
