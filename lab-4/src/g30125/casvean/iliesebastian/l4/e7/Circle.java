package g30125.casvean.iliesebastian.l4.e7;

public class Circle {
	private double radius=1.0;
	public static double pi=3.14;
	private String color="red";
	Circle()
	{
	
	}
	Circle(double radius)
	{
		this.radius=radius;
	}
	 double getRadius()
	{
		return this.radius;
	}
	double getArea()
	{
		return Math.pow(this.radius,2)*pi;
	}

}
