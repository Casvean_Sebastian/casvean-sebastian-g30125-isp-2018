package g30125.casvean.iliesebastian.l4.e6;

import org.junit.*;
import static org.junit.Assert.*;

public class TestAll {
	private Author[] authors;
	private Book newBook;
	
	@Test
	public void shouldReturnAuthors()
	{
	    authors = new Author[2];
		authors[0]=new Author("carag","mist@yahoo",'m');
		authors[1]=new Author("caterina","cater@yahoo",'f');
		newBook=new Book("By the tree",authors,25.99,3);
		assertEquals(authors,newBook.getAuthors());
	}

}
