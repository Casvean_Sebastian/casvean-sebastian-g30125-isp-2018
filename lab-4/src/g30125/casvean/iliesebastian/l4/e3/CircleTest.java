package g30125.casvean.iliesebastian.l4.e3;
import org.junit.*;
import static org.junit.Assert.*;

public class CircleTest {
	private Circle circle1;
	double pi=3.14;
	double check;
	@Before
	public void setUp()
	{
		circle1 = new Circle(2.3);
	}
	@Test
	public void shouldReturnRadius()
	{
		assertEquals(2.3,circle1.getRadius(),0);
	}
	
	@Test
	public void shouldReturnArea()
	{
		check=pi*Math.pow(circle1.getRadius(), 2);
		assertEquals(check,circle1.getArea(),0);
	}
}
