package g30125.casvean.iliesebastian.l6.e5;

import java.awt.Graphics;

public interface Shape {
	void draw(Graphics g);
}
