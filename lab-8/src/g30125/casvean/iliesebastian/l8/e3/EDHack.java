package g30125.casvean.iliesebastian.l8.e3;

import java.io.*;
import java.util.*;

public class EDHack {
	public static void main(String[] args) throws IOException
	{
		Scanner userInput=new Scanner(System.in);
		char choose;
		File f=new File("E:\\\\WORKSPACE\\\\Lab7Rezolvat\\\\src\\\\g30125\\\\casvean\\\\iliesebastian\\\\l8\\\\e3\\\\data.txt");
		File enc = new File("E:\\\\WORKSPACE\\\\Lab7Rezolvat\\\\src\\\\g30125\\\\casvean\\\\iliesebastian\\\\l8\\\\e3\\\\data.enc");
		File dec = new File("E:\\\\WORKSPACE\\\\Lab7Rezolvat\\\\src\\\\g30125\\\\casvean\\\\iliesebastian\\\\l8\\\\e3\\\\data.dec");
		System.out.println("(E)ncript / (D)ecript file : ");
		choose=userInput.next().charAt(0);
		if(choose=='E' || choose=='e') {
			String getLines;
			try
			{
				char[] converted;
				int ascii;
				BufferedReader fileR=new BufferedReader(new FileReader(f));
				PrintWriter fileW=new PrintWriter(new BufferedWriter(new FileWriter(enc)));
				getLines=fileR.readLine();
				while(getLines!=null)
				{
					converted=getLines.toCharArray();
					for(int i=0;i<converted.length;i++)
					{
						ascii=(int)converted[i];
						ascii--;
						fileW.print((char)ascii);
					}
					fileW.println();
					getLines=fileR.readLine();
				}
				fileW.close();
				fileR.close();
				
			}catch(IOException e)
			{
				e.printStackTrace();
			}
			System.out.println("File encripted with succes!");
		}// END OF ENCRIPT PICK;
		else if((choose=='D' || choose=='d') && (!dec.exists()))
		{
			String getLines;
			try {
				char[] converted;
				int ascii;
				BufferedReader fileR = new BufferedReader(new FileReader(enc));
				PrintWriter fileW= new PrintWriter(new BufferedWriter(new FileWriter(dec)));
				getLines=fileR.readLine();
				while(getLines != null)
				{
					converted=getLines.toCharArray();
					for(int i=0;i<converted.length;i++)
					{
						ascii=(int)converted[i];
						ascii++;
						fileW.print((char)ascii);
					}
					fileW.println();
					getLines=fileR.readLine();
					
				}
				fileW.close();
				fileR.close();
			}catch(IOException e)
			{
				e.printStackTrace();
			}
			System.out.println("File decripted with succes!");
		}//END OF DECRIPT PICK; 
		else
		{
			System.out.println("File is already decripted!");
		}
			
		
		
		
	}

}
