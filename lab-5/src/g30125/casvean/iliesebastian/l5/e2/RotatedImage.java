package g30125.casvean.iliesebastian.l5.e2;

public class RotatedImage implements Image {
	private RealImage realImage;
	private String fileName;
	public RotatedImage(String fileName)
	{
		this.fileName=fileName;
	}
	public void display()
	{
		if(realImage==null)
		{
			realImage=new RealImage(fileName);
			
		}
		System.out.println("Display rotated "+fileName);
	}
	
}
