package g30125.casvean.iliesebastian.l7.e1;
import java.util.*;
public class Bank{
	 ArrayList<BankAccount> accounts = new ArrayList<>();
	
	public void addAccount(String owner, double balance)
	{
		BankAccount b = new BankAccount(owner,balance);
		accounts.add(b);
	}
	public void printAccounts()
	{
		Collections.sort(accounts);
		Iterator<BankAccount> i = accounts.iterator();
		while(i.hasNext())
		{
			BankAccount b = i.next();
			System.out.println(b.getOwner());
		}
	}
	public void printAccounts(double minBalance, double maxBalance)
	{
		Iterator<BankAccount> i = accounts.iterator();
		while(i.hasNext())
		{
			BankAccount b = i.next();
			if(b.getBalance()>=minBalance && b.getBalance()<=maxBalance)
				System.out.println(b.getOwner()+" with balance= "+b.getBalance());
		}
	}
 
	public BankAccount getAccount(String owner)
	{
		Iterator<BankAccount>i=accounts.iterator();
		while(i.hasNext())
		{
			BankAccount b=(BankAccount)i.next();
			if(b.getOwner().equals(owner))
				return b;
		}
		return null;
	}
	static void displayAll(ArrayList list){
        
        Iterator<BankAccount>i = list.iterator();
        while(i.hasNext()){
              BankAccount b = (BankAccount)i.next();
              System.out.println(b.getOwner());
        }
  }
	public ArrayList<BankAccount> getAllAccounts()
	{
		return accounts;
	}
	public static void main(String[] args)
	{
		Bank b=new Bank();
		b.addAccount("Owner1",1234);
		b.addAccount("Owner2", 39435);
		b.addAccount("Ana", 24);
		b.addAccount("Mirel", 359.53);
		b.addAccount("Daniel", 943);
		b.addAccount("Cecilia", 3532);
		b.printAccounts();
		System.out.println("----------------");
		System.out.println("----------------");
		b.printAccounts(49, 1233);
		System.out.println("----------------");
		System.out.println("----------------");
		System.out.println(b.getAccount("Mirel"));
		System.out.println("----------------");
		System.out.println("----------------");
		ArrayList<BankAccount> Sorted=new ArrayList(b.getAllAccounts());
		Sorted.sort(new Comparator<BankAccount>()
				{

					@Override
					public int compare(BankAccount b1, BankAccount b2)
					{
						return b1.getOwner().compareTo(b2.getOwner());
					}
			
				});
		displayAll(Sorted);
		System.out.println("----------------");
		System.out.println("----------------");
		
		
		
		

	}
	

}
