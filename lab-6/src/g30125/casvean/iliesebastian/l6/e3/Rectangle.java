package g30125.casvean.iliesebastian.l6.e3;
import  g30125.casvean.iliesebastian.l6.e3.Shape;
import java.awt.Color;
import java.awt.Graphics;


public class Rectangle implements Shape{

    private int length;
    private int width;
    private Color color;
    private boolean filled;
    private int xCoord;
    private int yCoord;
    private String ID;

    public Rectangle(Color color, boolean filled,int xCoord,int yCoord,String ID,int length,int width) {
        this.color=color;
        this.filled=filled;
        this.xCoord=xCoord;
        this.yCoord=yCoord;
        this.ID=ID;
        this.length = length;
        this.width=width;
    }
    public Color getColor()
    {
    	return this.color;
    }
    
    public boolean isFilled()
    {
    	return this.filled;
    }
	public String getID() {
		
		return this.ID;
	
	}
    public void draw(Graphics g) 
    {
        System.out.println("Drawing a rectangle "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(xCoord,yCoord,width,length);
        if(this.isFilled())
        {
        	g.fillRect(xCoord,yCoord, width,length);
        }
    }

}