package g30125.casvean.iliesebastian.l3.e6;

import java.util.*;
public class MyPoint {
	private int x,y;
	MyPoint()
	{
		this.x=0;
		this.y=0;
	}
	MyPoint(int xCoord,int yCoord)
	{
		this.x=xCoord;
		this.y=yCoord;
	}
	int getX()
	{
		return this.x;
	}
	int getY()
	{
		return this.y;
	}
	void setXY(int x,int y)
	{
		this.x=x;
		this.y=y;
	}
	String ToString()
	{
		return "("+this.x+","+this.y+")";
	}
	double distance(int x,int y)
	{
		double sum1=0,sum2=0;
		sum2=Math.pow(x-this.x,2) + Math.pow(y-this.y, 2);
		sum1=Math.sqrt(sum2);
		return sum1;
		
	}
	double distance(MyPoint anotherPoint)
	{
		double sum1=0,sum2=0;
		sum2=Math.pow(anotherPoint.getX()-this.x, 2)+Math.pow(anotherPoint.getY()-this.y,2);
		sum1=Math.sqrt(sum2);
		return sum1;
	}
}