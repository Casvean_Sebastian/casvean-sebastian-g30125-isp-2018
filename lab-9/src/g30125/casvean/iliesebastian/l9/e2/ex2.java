package g30125.casvean.iliesebastian.l9.e2;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ex2 extends JFrame{
	private JButton button;
	private JLabel label;
	private static int counter=0;
	ex2()
	{
		this.setTitle("Counter");
		this.setSize(400,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		init();
		this.setVisible(true);
	}
	public void init()
	{
		setLayout(new GridLayout(2, 1));
		button=new JButton("Click me!");
		this.add(button);
		label=new JLabel("");
		this.add(label);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getSource()==button)
				{
					counter++;
					String stringcounter=Integer.toString(counter);
					label.setText(stringcounter);
				}
				
				
			}
			
		});
	}
	public static void main(String[] args)
	{
		new ex2();
	}
}
