package g30125.casvean.iliesebastian.l4.e4;

public class Author {
	private String name;
	private String email;
	private char gender;
	public Author(String name, String email, char gender)
	{
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	String getName()
	{
		return this.name;
	}
	String getEmail()
	{
		return this.email;
	}
	char getGender()
	{
		return this.gender;
	}
	void setEmail(String email)
	{
		this.email=email;
	}
	public String toString()
	{
		return this.name+" "+this.gender+" at "+this.email;
	}
	

}
