package g30125.casvean.iliesebastian.l4.e8;

public class Rectangle extends Shape{
	private double width=1.0;
	private double length=1.0;
	Rectangle()
	{
		
	}
	Rectangle(double width, double length)
	{
		this.width=width;
		this.length=length;
	}
	Rectangle(double width, double length, String color, boolean filled)
	{
		super(color,filled);
		this.width=width;
		this.length=length;
	}
	double getWidth()
	{
		return this.width;
	}
	void setWidth(double width)
	{
		this.width=width;
	}
	double getLength()
	{
		return this.length;
	}
	void setLength(double length)
	{
		this.length=length;
	}
	double getArea()
	{
		return this.width*this.length;
	}
	double getPerimeter()
	{
		return this.width*2+this.length*2;
	}
	public String toString()
	{
		return "a rectangle with width="+getWidth()+" and length="+getLength()+" which is a subclass of "+super.toString();
	}

}
