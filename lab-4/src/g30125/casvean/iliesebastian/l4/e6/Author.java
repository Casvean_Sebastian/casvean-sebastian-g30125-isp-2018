package g30125.casvean.iliesebastian.l4.e6;

public class Author {
	private String name;
	private String email;
	private char gender;
	 Author(String name, String email, char gender)
	{
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	String getName()
	{
		return this.name;
	}
	String getEmail()
	{
		return this.email;
	}
	char getGender()
	{
		return this.gender;
	}
	void setEmail(String email)
	{
		this.email=email;
	}
	public String toString()
	{
		return this.name+" "+this.gender+" at "+this.email;
	}
	

}