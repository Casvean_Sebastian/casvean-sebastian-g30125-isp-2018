package g30125.casvean.iliesebastian.l6.e3;

import java.awt.Color;
import java.awt.Graphics;



public class Circle	implements Shape{

    private int radius;
    private Color color;
    private boolean filled;
    private int xCoord;
    private int yCoord;
    private String ID;

    public Circle(Color color, boolean filled,int xCoord,int yCoord,String ID,int radius) {
    	this.color=color;
        this.filled=filled;
        this.xCoord=xCoord;
        this.yCoord=yCoord;
        this.ID=ID;
        this.radius = radius;
    }
    public Color getColor()
    {
    	return this.color;
    }
    public boolean isFilled()
    {
    	return this.filled;
    }
    public String getID()
    {
    	return this.ID;
    }
    

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(xCoord,yCoord,radius,radius);
        if(this.isFilled())
        {
        	g.fillOval(xCoord,yCoord, radius, radius);
        }
    }
}