package g30125.casvean.iliesebastian.l4.e8;

public class Shape {
	private String color="red";
	private boolean filled=true;
	Shape()
	{
		
	}
	Shape(String color, boolean filled)
	{
		this.color=color;
		this.filled=filled;
	}
	String getColor()
	{
		return this.color;
	}
	void setColor(String color)
	{
		this.color=color;
	}
	boolean isFilled()
	{
		return this.filled;
	}
	void setFilled(boolean filled)
	{
		this.filled=filled;
	}
	public String toString()
	{
		if(isFilled())
		return "a shape with color of "+getColor()+" and filled";
		else 
			return "a shape with color of "+getColor()+" and notFilled";
	}

}
