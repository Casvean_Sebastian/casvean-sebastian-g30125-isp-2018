package g30125.casvean.iliesebastian.l10.e2;


class Board {
    int x,y;
	int[][] board;
	Board (int x,int y)
	{
		this.x=x;
		this.y=y;
		board = new int[x][y];
	}
	public int[][] returnBoard()
	{
		return board;
	}
}
public class Robot extends Thread{
	private boolean running=true;
	
	private Board b;
	private int x,y;
	private String name;
	private int checkX,checkY;
	Robot(String name,int x,int y,Board b)
	{
		this.b=b;
		this.name=name;
		this.x=x;
		this.y=y;
		b.returnBoard()[x][y]=1;
	}
	
	public void run()
	{
		while(running)
		{
			
			int i=(int)(Math.random()*100);
			if(i>0&&i<15)
			{
			checkX=x;
			checkY=y;
			
			if(checkX-->0)
			{
				x--;
			}
			if(checkY++<b.y)
			{
				y++;
			}
			System.out.println(name+" --->>>> "+"["+x+"]["+y+"] ");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			}//END LEFT-UP
			else if(i>15&&i<30)
			{
			checkX=x;
			checkY=y;
			if(checkX-->0)
			{
				x--;
			}
			if(checkY-->0)
			{
				y--;
			}
			System.out.println(name+"--->>>> ["+x+"]["+y+"] ");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			}//END LEFT-DOWN
			else if(i>30&&i<45)
			{
			checkX=x;
			checkY=y;
			if(checkX++<b.x)
			{
				x++;
			}
			if(checkY++<b.y)
			{
				y++;
			}
			System.out.println(name+"--->>>> "+"["+x+"]["+y+"] ");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			}//END RIGHT-UP
			else
				if(i>45&&i<70)
				{
				checkX=x;
				checkY=y;
				if(checkX++<b.x)
				{
					x++;
				}
				if(checkY-->0)
				{
					y--;
				}
				System.out.println(name+" --->>>> "+"["+x+"]["+y+"] ");
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				}//END RIGHT-DOWN 
			synchronized(b)
			{
				(b.returnBoard())[checkX][checkY]=0;
				(b.returnBoard())[x][y]=1;
			}
			
			//END MOVING CONDITIONS FOR THE ROBOT
			
		}//END WHILE
	}//END RUN

	public static void main(String[] args)
	{
		Board b= new Board(100,100);
		Robot r1=new Robot("ROBOT1",10,54,b);
		Robot r2=new Robot("ROBOT2",52,23,b);
		r1.start();
		r2.start();
	}
}
