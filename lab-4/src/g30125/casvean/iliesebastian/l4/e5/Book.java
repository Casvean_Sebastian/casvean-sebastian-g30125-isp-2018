package g30125.casvean.iliesebastian.l4.e5;
import g30125.casvean.iliesebastian.l4.e4.Author;

public class Book {
	private String name;
	private Author author;
	private double price;
	private int qtyInStock=0;
	Book(String name, Author author, double price)
	{
		this.name=name;
		this.author=author;
		this.price=price;
	}
	Book(String name, Author author, double price, int qtyInStock)
	{
		this.name=name;
		this.author=author;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}
	String getName()
	{
		return this.name;
	}
	Author getAuthor()
	{
		return this.author;
	}
	double getPrice()
	{
		return this.price;
	}
	void setPrice(double price)
	{
		this.price=price;
	}
	int getQtyInStock()
	{
		return this.qtyInStock;
	}
	void setQtyInStock(int qtyInStock)
	{
		this.qtyInStock=qtyInStock;
	}
	public String toString()
	{
		return getName()+" by "+getAuthor().toString();
	}
	public static void main(String[] args)
	{
		Author author1 = new Author("Caragiale","caragiale@yahoo.com",'m');
		Book booktest = new Book("In ape adanci",author1,43.535);
		System.out.println(booktest.toString());
	}
	
}
