package g30125.casvean.iliesebastian.l4.e2;

import org.junit.Test;

import static org.junit.Assert.*;

public class MyPointTest {
	
	
	@Test
	public void shouldReturnX()
	{
		MyPoint firstPoint=new MyPoint(5,10);
		assertEquals(firstPoint.getX(),5);
	}
	
	@Test
	public void shouldSetXY()
	{
		MyPoint secondPoint= new MyPoint();
		secondPoint.setXY(2, 3);
		assertEquals(secondPoint.getX(),2);
		assertEquals(secondPoint.getY(),3);
		
	}
}
