package g30125.casvean.iliesebastian.l4.e4;
import org.junit.*;

import static org.junit.Assert.*;

public class AuthorTest {
	private Author author1;
	@Before
	public void setUp()
	{
	        author1 = new Author("Caragiale","caragiale@yahoo.com",'m');
	}
	@Test
	public void shouldReturnName()
	{
	
		assertEquals("Caragiale",author1.getName());
	}
	
	@Test
	public void shouldReturnEmail()
	{
		
		assertEquals("caragiale@yahoo.com",author1.getEmail());
	}
	@Test
	public void shouldReturnGender()
	{
		assertEquals('m',author1.getGender());
	}
	@Test
	public void shouldSetEmail()
	{
		author1.setEmail("emailnou@yahoo.com");
		assertEquals("emailnou@yahoo.com",author1.getEmail());
	}
	@Test
	public void shoulReturnAll()
	{
		assertEquals("Caragiale m at caragiale@yahoo.com",author1.toString());
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
