package g30125.casvean.iliesebastian.l10.e1;

public class Counter extends Thread{
	Thread t;
	String name;
	static int j=0;
	Counter(Thread t,String name)
	{
		this.name=name;
		this.t=t;
	}
	public void run() {
		
		try {
			
			if(t!=null) 
			t.join();
			for(int i=0;i<=100;i++)
			{
				System.out.println(this.name+" : "+(i+j));
				if(i==100) j=100;
				
				Thread.sleep(10);
			}
		 	}catch(Exception e)
		{
		 		e.printStackTrace();
		}
		
	}
	public static void main(String[] args)
	{
		Counter c1=new Counter(null,"counter 1");
		Counter c2=new Counter(c1,"counter 2");
		c1.start();
		c2.start();
		
	}

}
