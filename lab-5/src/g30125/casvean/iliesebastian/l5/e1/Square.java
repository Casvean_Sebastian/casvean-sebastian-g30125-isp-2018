package g30125.casvean.iliesebastian.l5.e1;

public class Square extends Rectangle{
	public Square()
	{
		
	}
	public Square(double side)
	{
		super(side,side);
	}
	public Square(double side,String color,boolean filled)
	{
		super(side,side,color,filled);
	}
	public double getSide()
	{
		return super.getLength();
	}
	public void setSide(double side)
	{
		super.width=side;
		super.length=side;
	}
	public String toString()
	{
		return "Square";
	}
	
}
