package g30125.casvean.iliesebastian.l4.e8;
import org.junit.*;
import static org.junit.Assert.*;

public class TestClase {
	private Rectangle test=new Rectangle(1,2,"blue",false);
	
	@Test
	public void shouldReturnString()
	{
		assertEquals("a rectangle with width=1.0 and length=2.0 which is a subclass of a shape with color of blue and notFilled",test.toString());
	}

}