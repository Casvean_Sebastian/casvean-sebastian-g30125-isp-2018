package g30125.casvean.iliesebastian.l9.e4;

import java.awt.event.*;
import java.awt.*;

import javax.swing.*;

public class ex4 extends JFrame {
	private JButton button;
	private boolean found=false;
	private int finish=0;
	private boolean Player=false;
	private boolean Computer=false;
	private JTextArea[][] tf;
	int[][] checked = new int[3][3];
	ex4()
	{
		this.setTitle("x & 0");
		this.setSize(400,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		init();
		this.setVisible(true);
	}
	public void init()
	{
		this.setLayout(null);
		int width=20;
		int height=20;
		tf=new JTextArea[3][3];
		for(int i=0;i<tf.length;i++)
		{
			for(int j=0;j<tf[i].length;j++)
			{
				
				checked[i][j]=0;
				tf[i][j]=new JTextArea("");
				tf[i][j].setBounds(30+i*25,30+j*25,width,height);
			}
			
		}
		
		button=new JButton("set");
		button.setBounds(120,35,90,50);
		button.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(e.getSource()==button )
				{
					
					for(int i=0;i<ex4.this.tf.length;i++)
					{
						for(int j=0;j<ex4.this.tf[i].length;j++)
						{
							if(ex4.this.tf[i][j].getText().equals("x") && checked[i][j]==0) //FIND THE NEW x's place 
							{
								checked[i][j]=1;
								finish++;
								while(!found && finish<9) // finish<9 : condition for the last box checking
								{
								int randomi=(int)(Math.random()*3);
								int randomj=(int)(Math.random()*3);
									if(!((ex4.this.tf[randomi][randomj].getText()).equals("x")) && !((ex4.this.tf[randomi][randomj].getText()).equals("0")))
									{
										ex4.this.tf[randomi][randomj].append("0");
										checked[randomi][randomj]=1;
										finish++;
										found=true;
									}
								}
								found=false;
							}
						}
						
					}//END ADDING 0's
					for(int i=0;i<3;i++)
					{
							if(ex4.this.tf[i][0].getText().equals(ex4.this.tf[i][1].getText()) && ex4.this.tf[i][0].getText().equals(ex4.this.tf[i][2].getText()) )
							{
								if(ex4.this.tf[i][0].getText().equals("x"))
								{
									Player=true;
									break;
								}
								else if(ex4.this.tf[i][0].getText().equals("0"))
								{
									Computer=true;
									break;
								}
								
							}
					}//END COLUMN CHECK
					
					for(int i=0;i<3;i++)
					{
						if(ex4.this.tf[0][i].getText().equals(ex4.this.tf[1][i].getText()) && ex4.this.tf[0][i].getText().equals(ex4.this.tf[2][i].getText()) )
						{
							if(ex4.this.tf[0][i].getText().equals("x"))
							{
								Player=true;
								break;
							}
							else if(ex4.this.tf[0][i].getText().equals("0"))
							{
								Computer=true;
								break;
							}
							
						}
					}//END LINE CHECK
					
						if(ex4.this.tf[0][0].getText().equals(ex4.this.tf[1][1].getText()) && ex4.this.tf[0][0].getText().equals(ex4.this.tf[2][2].getText()) )
						{
							if(ex4.this.tf[0][0].getText().equals("x"))
							{
								Player=true;
							}
							else if(ex4.this.tf[0][0].getText().equals("0"))
							{
								Computer=true;
							}
							
						}//END PRINCIPAL DIAGONAL CHECK
						if(ex4.this.tf[0][2].getText().equals(ex4.this.tf[2][2].getText()) && ex4.this.tf[0][2].getText().equals(ex4.this.tf[2][0].getText()) )
						{
							if(ex4.this.tf[0][2].getText().equals("x"))
							{
								Player=true;
							}
							else if(ex4.this.tf[0][2].getText().equals("0"))
							{
								Computer=true;
							}
							
						}//END SECONDARY DIAGONAL CHECK
						
					
				if(Player)
						JOptionPane.showMessageDialog(null, "X wins");	
					else if(Computer)
						JOptionPane.showMessageDialog(null, "0 wins");	
					else if(finish==9)
						JOptionPane.showMessageDialog(null, "Draw");	
						
					
				}//FINISH e.GetSource()
				
			}//FINISH ACTION_PERFORMED
			
		});
		this.add(button);
		
		for(int i=0;i<tf.length;i++)
		{
			for(int j=0;j<tf[i].length;j++)
			{
				this.add(tf[i][j]);
			}
			
		}
	}
	public void findSpot()
	{
		
	}
	public static void main(String[] args)
	{
		new ex4();
		
	}
}
