package g30125.casvean.iliesebastian.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    private int width;

    public Rectangle(Color color, boolean filled,int xCoord,int yCoord,String ID,int length,int width) {
        super(color,filled,xCoord,yCoord,ID);
        this.length = length;
        this.width=width;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(this.getX(),this.getY(),width,length);
        if(this.isFilled())
        {
        	g.fillRect(this.getX(), this.getY(), width,length);
        }
    }
}
