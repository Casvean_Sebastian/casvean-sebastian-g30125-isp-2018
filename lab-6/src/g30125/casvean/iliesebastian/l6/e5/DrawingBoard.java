package g30125.casvean.iliesebastian.l6.e5;

import java.awt.*;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.*;

public class DrawingBoard extends JFrame{
	public static int NUM=20,sum=0,rows=0;
	Shape[] shapes=new Shape[99];


	public DrawingBoard() 
	{
	this.setTitle("Drawing board example");
    this.setSize(300,500);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setLayout(new FlowLayout());
	this.setVisible(true);
  
}
	public void paint(Graphics g)
	{
		
		for(int i=0;i<shapes.length;i++)
		{
			if(shapes[i]!=null)
				shapes[i].draw(g);
		}
		
	}
	
	

	public void addBrick(Shape b1)
	{
		for(int i=0;i<shapes.length;i++)
		{
			if(shapes[i]==null)
			{
				shapes[i]=b1;
			break;
			}
			
		}
		this.repaint();
	}
	
	public static void main(String[] args) 
	{
		
		DrawingBoard newdb=new DrawingBoard();
		int i,j,k=0;
		Shape[] bricks=new Brick[NUM];
		
		for(i=0;i<NUM;i++)
		{
			if(sum+i+1<=NUM)
			{
				sum+=i+1;
				rows=i+1;
			}
		}
		
		for(i=rows-1;i>=0;i--)
		{
			for(j=0;j<=i;j++)
			{
				bricks[k]=new Brick(20+(j+1)*30+(rows-(i+1))*15,300-(rows-(i+1))*10,30,10);
				k++;
			}
		}
		
		for(i=0;i<sum;i++)
		{
			
			newdb.addBrick(bricks[i]);
			
		}
		
		
		
		
		
	}

}