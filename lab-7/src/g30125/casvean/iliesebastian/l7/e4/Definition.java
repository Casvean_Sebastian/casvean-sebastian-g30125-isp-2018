package g30125.casvean.iliesebastian.l7.e4;

import java.util.Objects;

public class Definition {
	private String description;
	public Definition(String description)
	{
		this.description=description;
	}
	public String toString()
	{
		return this.description;
	}
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Definition)) return false;
        Definition d = (Definition) o;
        return Objects.equals(description,d.description);
    }
	
    @Override
    public int hashCode() {
        return Objects.hash(description);
    }
}
